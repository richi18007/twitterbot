# Twitter Bot #

A simple bot which can :-

1. Find out the twitter trends
2. Post Messages on Twitter timeLines
3. Find out the search results 
4. Reply to tweets 

### What is this repository for? ###

* Uses the twpython library to do all the awesome stuff with twitter
* 0.0.1

### How do I get set up? ###

* Set up virtualenv (pip install virtualenv)
* virtualenv env
* cd env
* source ~/bin/activate
* git clone -b git@bitbucket.org:richi18007/twitterbot.git
* pip install -r < requirements.txt
* python run bot.py (Look at the instructions to run the file properly)


### Who do I talk to? ###

* Srivastava , Sankalp < richi18007@gmail.com >
* Ish Jindal < ish.jindal@gmail.com >